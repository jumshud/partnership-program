# Http
from django.http import HttpResponse, Http404
from django.views import generic as generic_views
from django.core.serializers.json import DjangoJSONEncoder
import json
# models
from core.models import PartnershipProgram, AccessLevel, PartnershipRequest
from partnership_program.models import ARCHIVE_PAGE_SIZE
#functions
from core.functions import paginate, get_pages
# Template
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
# Authentication
from django.contrib.auth.decorators import login_required

class PublishedProgramMixin(object):
    """
    Since the queryset also has to filter elements by timestamp
    we have to fetch it dynamically.
    """
    def get_queryset(self):
        return PartnershipProgram.objects.all()


class ArchiveIndexView(PublishedProgramMixin, generic_views.ListView):
    template_name = 'partnership_program/all_programs.html'
    date_field = 'update_date'

    def get_context_data(self, **kwargs):
        items = self.get_queryset()
        pages = get_pages(self.request,items,ARCHIVE_PAGE_SIZE)

        page_count = len(pages.paginator.page_range)
        current_page = pages.number
        page_list = paginate(ARCHIVE_PAGE_SIZE,page_count,current_page)

        context = super(ArchiveIndexView, self).get_context_data(**kwargs)
        context['pages'] = pages
        context['page_list'] = page_list
        context['page_count'] = page_count

        return context


# partnership program detail
def program_details(request,slug):
    part_program = get_object_or_404(PartnershipProgram, slug=slug)
    latest_programs= PartnershipProgram.objects.order_by("-update_date").exclude(pk=part_program.pk)[:6]
    program_materials = get_object_or_404(AccessLevel, name='Public').programmaterials.all()
    request_sent = False
    if request.user.is_authenticated():
        request_sent = PartnershipRequest.objects.filter(partnership_program=part_program, partner=request.user.partner)
    created = 0
    if request.method == 'POST':
        if request.user.is_authenticated():
            request_obj = PartnershipRequest()
            request_obj.partnership_program = part_program
            request_obj.partner = request.user.partner
            request_obj.status_id = 1
            request_obj.save()
            created = 1
        else:
            next_url = redirect('partnership_program:details',part_program.slug)['Location']
            login_url = "/account/login/?next=%s" % next_url
            return redirect(login_url)



    return render_to_response('partnership_program/partnership_detail.html',
                              {'part_program':part_program,'latest_programs':latest_programs,
                               'program_materials':program_materials,'created':created,
                               'request_sent':request_sent},
                              context_instance=RequestContext(request))


