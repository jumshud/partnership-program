from django import forms

#widgets
from ckeditor.widgets import CKEditorWidget
#model
from core.models import PartnershipProgramMaterial, PartnershipProgram


class PartnershipProgramForm(forms.ModelForm):

    class Meta:
        model = PartnershipProgram

    def __init__(self, *args, **kwargs):
        super(PartnershipProgramForm, self).__init__(*args, **kwargs)
        self.fields['desc'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style': 'resize:none;'})
        # self.fields['text_en'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
        # self.fields['text_ru'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
        # self.fields['text_tr'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})

class PartnershipProgramMaterialForm(forms.ModelForm):

    class Meta:
        model = PartnershipProgramMaterial

    def __init__(self, *args, **kwargs):
        super(PartnershipProgramMaterialForm, self).__init__(*args, **kwargs)
        self.fields['text'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style': 'resize:none;'})
        # self.fields['text_en'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
        # self.fields['text_ru'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
        # self.fields['text_tr'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})

    def clean_file(self):
        file = self.cleaned_data['file']
        text = self.cleaned_data['text']
        if (file and text) or (not file and not text):
            raise forms.ValidationError('You must select file or text.')

        return file
