from django.contrib import admin
from core.models import PartnershipProgram, PartnershipProgramMaterial, AccessLevel
from partnership_program.forms import PartnershipProgramMaterialForm, PartnershipProgramForm


class PartnershipProgramAdmin(admin.ModelAdmin):
    list_display = ('name', 'desc', 'image', 'create_date', 'update_date')
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ('name','create_date', 'update_date', 'category')
    form = PartnershipProgramForm
    filter_horizontal = ('category',)
admin.site.register(PartnershipProgram, PartnershipProgramAdmin)

class PartnershipProgramMaterialAdmin(admin.ModelAdmin):
    list_display = ('name', 'desc', 'text', 'file', 'access_level')
    search_fields = ('name','access_level')
    form = PartnershipProgramMaterialForm
admin.site.register(PartnershipProgramMaterial, PartnershipProgramMaterialAdmin)

class AccessLevelAdmin(admin.ModelAdmin):
    list_display = ('name', )
admin.site.register(AccessLevel, AccessLevelAdmin)