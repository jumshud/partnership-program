from django.conf.urls import patterns, url
from partnership_program import views

from partnership_program.models import ARCHIVE_PAGE_SIZE


urlpatterns = patterns('',
    url(r'^$',
        views.ArchiveIndexView.as_view(paginate_by=ARCHIVE_PAGE_SIZE), name='all'),
    url(r'^(?P<slug>[-\w]+)/$',  views.program_details,  name='details'),
)