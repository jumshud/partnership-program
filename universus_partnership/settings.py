"""
Django settings for universus_partnership project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


ugettext = lambda s: s

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'j!#7=89f9w8(@=6d(e&a(6h$-ew%l-reap$ipkf)v7u6q3xfj@'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

LOGIN_URL = '/account/login'



# Application definition

INSTALLED_APPS = (
    'modeltranslation',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'south',
    'ckeditor',
    'sendfile',

    #universus_partnership apps
    'core',
    'news',
    'page',
    'partnership_program',
    'workspace',
    'vacancy',
)

#CKEditor
CKEDITOR_UPLOAD_PATH = 'media-uploads/'
CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Complete',
        # 'toolbar': 'Full',
        'toolbar_Complete': [
            ['Source', '-', 'Preview'], ['Paste', 'Paste as plain text'],
        ],

        'toolbar_Complete': [
            { 'name': 'document', 'groups': [ 'mode', 'document', 'doctools' ], 'items': [ 'Source', '-', 'Preview'] },
            { 'name': 'clipboard', 'groups': [ 'clipboard', 'undo' ], 'items': ['Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
            { 'name': 'editing', 'groups': [ 'find', 'selection', 'spellchecker' ], 'items': ['Scayt' ] },
            { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ], 'items': [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
            { 'name': 'paragraph', 'groups': [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 'items': [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
            { 'name': 'links', 'items': [ 'Link', 'Unlink', 'Anchor' ] },
            { 'name': 'insert', 'items': [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar', 'Iframe' ] },
            '/',
            { 'name': 'styles', 'items': [ 'Styles', 'Format', 'FontSize' ] },
            { 'name': 'colors', 'items': [ 'TextColor', 'BGColor' ] },
            { 'name': 'tools', 'items': [ 'Maximize', 'ShowBlocks' ] },
            { 'name': 'others', 'items': [ '-' ] },
        ],
        # 'height': 300,
        # 'width': '90%',
        # 'extraPlugins': 'image2',
        'removePlugins': 'stylesheetparser',
    },
    'awesome_ckeditor': {
        'toolbar': 'Basic',
    },
}
CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

AUTH_USER_MODEL = 'core.User'

MIDDLEWARE_CLASSES = (
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'django.middleware.common.CommonMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
    'django.core.context_processors.debug',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.csrf',
    'django.core.context_processors.tz',
    'sekizai.context_processors.sekizai',
    'django.core.context_processors.static',
)


ROOT_URLCONF = 'universus_partnership.urls'

WSGI_APPLICATION = 'universus_partnership.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'universus_partnership',
        'USER': 'postgres',
        'PASSWORD': 'hsa6z8wx',
        'HOST': '192.168.137.2',
        'PORT': '5432',
    }
}

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
#
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'univpartship',
#         'USER': 'openpg',
#         'PASSWORD': 'openpgpwd',
#         'HOST': '',
#         'PORT': '5432',
#     }
# }
#
#


LANGUAGE_CODE = 'en'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

# Language conf
LANGUAGES = (
    ('en', ugettext(u'English')),
    ('ru', ugettext(u'Russian')),
    ('tr', ugettext(u'Turkish')),
)

MODELTRANSLATION_LANGUAGES = ('en', 'ru','tr')
MODELTRANSLATION_DEFAULT_LANGUAGE = 'en'

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

#Template directory
TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

#STATIC_ROOT = os.path.join(BASE_DIR, 'static')
# Static file url
STATIC_URL = '/static/'

# Static files folder
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

#Media files
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

#Media files url
MEDIA_URL = '/media/'


SENDFILE_ROOT = os.path.join(BASE_DIR, "private")
SENDFILE_URL = '/private/'

SENDFILE_BACKEND = 'sendfile.backends.development'

#Partnership portal url
PORTAL_URL = 'www.partnership.com/'

#email configuration
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'saleportal159@gmail.com'
EMAIL_HOST_PASSWORD = 'saleportal71123'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
