"""
WSGI config for universus_partnership project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""


import sys, os

project = os.path.dirname(os.path.dirname(__file__))
sys.path.append(project)

os.environ['DJANGO_SETTINGS_MODULE'] = 'universus_partnership.settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
