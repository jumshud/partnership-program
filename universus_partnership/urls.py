from django.conf.urls import patterns, include, url

from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings

from django.contrib.staticfiles.urls import staticfiles_urlpatterns



admin.autodiscover() 
urlpatterns = i18n_patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(r'^news/', include('news.urls', namespace='news')),
    url(r'^partnership-program/', include('partnership_program.urls', namespace='partnership_program')),
    url(r'^vacancy/', include('vacancy.urls', namespace='vacancy')),

    url(r'^', include('core.urls', namespace='core')),
)

# ... the rest of your URLconf goes here ...

urlpatterns += staticfiles_urlpatterns()


if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'', include('django.contrib.staticfiles.urls')),
) + urlpatterns