from django import forms

#widgets
from ckeditor.widgets import CKEditorWidget
#model
from .models import News


class NewsForm(forms.ModelForm):

    class Meta:
        model = News

    def __init__(self, *args, **kwargs):
        super(NewsForm, self).__init__(*args, **kwargs)
        self.fields['content'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
        self.fields['content_en'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
        self.fields['content_ru'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
        self.fields['content_tr'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
