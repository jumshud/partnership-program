from django.views import generic as generic_views
from django.template import RequestContext

from . import models
from models import News
from settings import ARCHIVE_PAGE_SIZE

from core.functions import paginate, get_pages

from news.settings import ARCHIVE_PAGE_SIZE

class PublishedNewsMixin(object):
    """
    Since the queryset also has to filter elements by timestamp
    we have to fetch it dynamically.
    """
    def get_queryset(self):
        return models.News.published.all()



class ArchiveIndexView(PublishedNewsMixin, generic_views.ListView):
    template_name = 'news/news_archive.html'
    date_field = 'pub_date'

    def get_context_data(self, **kwargs):
        items = self.get_queryset()
        pages = get_pages(self.request,items,ARCHIVE_PAGE_SIZE)

        page_count = len(pages.paginator.page_range)
        current_page = pages.number
        page_list = paginate(ARCHIVE_PAGE_SIZE,page_count,current_page)

        context = super(ArchiveIndexView, self).get_context_data(**kwargs)
        context['pages'] = pages
        context['page_list'] = page_list
        context['page_count'] = page_count

        return context



class DetailView(PublishedNewsMixin, generic_views.DetailView):
    month_format = '%m'
    date_field = 'pub_date'


	
