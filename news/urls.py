# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings

from . import views
from news.settings import ARCHIVE_PAGE_SIZE




urlpatterns = patterns('django.views.generic.date_based',
    url(r'^$', #paginate_by must be egual 
        views.ArchiveIndexView.as_view(paginate_by=ARCHIVE_PAGE_SIZE), name='news_archive_index'),

    url(r'^(?P<slug>[-\w]+)/$',
        views.DetailView.as_view(), name='news_detail'),

)
