from django.utils.timezone import now as datetime_now

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from news import settings

class PublishedNewsManager(models.Manager):
    """
        Filters out all unpublished and items with a publication date in the future
    """
    def get_query_set(self):
        return super(PublishedNewsManager, self).get_query_set() \
                    .filter(is_published=True) \
                    .filter(pub_date__lte=datetime_now())


class News(models.Model):
    image = models.ImageField(_('image'), upload_to='news/')
    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'), unique_for_date='pub_date',
                        help_text=_('A slug is a short name which uniquely identifies the news item'))
    content = models.TextField(_('Content'), blank=True)

    is_published = models.BooleanField(_('Published'), default=False)
    pub_date = models.DateField(_('Publication date'), default=datetime_now)

    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    published = PublishedNewsManager()
    objects = models.Manager()


    class Meta:
        verbose_name = _('News')
        verbose_name_plural = _('News')
        ordering = ('-pub_date', )

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('news:news_detail', kwargs={'slug': self.slug})
