from modeltranslation.translator import translator, TranslationOptions
from ckeditor.widgets import CKEditorWidget
from news.models import News
from django import forms

class NewsTranslationOptions(TranslationOptions):
    fields = ('title','slug','content')
    class Meta:
        model = News
        widgets = {
           'content' : forms.Textarea(attrs={'class':'ckeditor'}),
        }


translator.register(News,NewsTranslationOptions)
