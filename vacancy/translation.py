from modeltranslation.translator import translator, TranslationOptions
from ckeditor.widgets import CKEditorWidget
from vacancy.models import Vacancy
from django import forms


class VacancyTranslationOptions(TranslationOptions):
    fields = ('name','salary_description','education','experience','job_description')

# class PageTranslationOptions(TranslationOptions):
#     fields = ('title','slug','content')
#     class Meta:
#         model = Page
#         widgets = {
#            'content' : forms.Textarea(attrs={'class':'ckeditor'}),
    #        # 'field_2' : forms.Textarea(attrs={'class':'ckeditor'}),
    #     }


translator.register(Vacancy,VacancyTranslationOptions)
# translator.register(Page,PageTranslationOptions)
