from django.views import generic as generic_views
# from django.views import FormMixin, ListView
# from . import models,settings
# from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.template import RequestContext

from django.shortcuts import render,render_to_response
from django.http import HttpResponse
from django.shortcuts import render
# Create your views here.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~BOOK CATEGORY LIST~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from django.http import HttpResponseRedirect,Http404
# from .forms import UploadFileForm
from django.utils import translation
from models import Vacancy,VacancyApplication
from forms import VacancyApplicationForm
from django.utils import timezone
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core.context_processors import csrf
from django.core.mail import send_mail, BadHeaderError
# from universus_public.settings import EMAIL_HOST_USER
from django.db.models import Q
# Create your views here.
from core.models import User
from django.views.generic.edit import FormMixin
from django.views.generic.list import ListView

def vacancy_list(request):
    template_name = 'vacancy/vacancy_list.html'
    cur_language=request.LANGUAGE_CODE
    vacancy_list = Vacancy.objects.all()
    default_url=1


    return render(request,'vacancy/vacancy_list.html',{'cur_language':cur_language,'vacancy_list':vacancy_list,'default_url':default_url})

class VacancyDetailView(FormMixin, ListView):
    model = Vacancy
    template_name = 'vacancy/vacancy_detail.html'
    form_class = VacancyApplicationForm
    success_url = '/thanks/'

    def get(self, request, *args, **kwargs):
     # From ProcessFormMixin
        form_class = self.get_form_class()
        self.form = self.get_form(form_class)

        if self.request.method == 'POST':
            # return HttpResponse(self.form.errors)
            self.form_class(request.POST, request.FILES)
            if self.form.is_valid():
                obj = self.form.save(commit=False)
                # obj.save()
#             return HttpResponseRedirect('/thanks/')
        else:
            self.form_class(self.request.POST,request.FILES)

            # return super(ProductListView, self).form_valid(form)

        # From BaseListView
        self.object_list = self.get_queryset()
        # allow_empty = self.get_allow_empty()
        # if not allow_empty and len(self.object_list) == 0:
        #     raise Http404(_(u"Empty list and '%(class_name)s.allow_empty' is False.")
        #                   % {'class_name': self.__class__.__name__})

        context = self.get_context_data(object_list=self.object_list, form=self.form)
        # context = super(VacancyDetailView, self).get_context_data(**kwargs)
        context['slug'] = Vacancy.objects.all()
        vacancy_obj = Vacancy.objects.get(slug=self.kwargs['slug'])
        context['vacancy'] = vacancy_obj
        context['vacancy_list']=Vacancy.objects.filter(~Q(slug=self.kwargs['slug']))
        cur_language = translation.get_language()
        context['cur_language'] = cur_language
        context['default_url'] = 1
        context['x'] = self.request.user.id
        context['y'] = vacancy_obj.id


        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        # When the form is submitted, it will enter here
        # self.object = None
        self.form = self.get_form(self.form_class)

        if self.form.is_valid():
            self.form.save()
            # Here ou may consider creating a new instance of form_class(),
            # so that the form will come clean.

        # Whether the form validates or not, the view will be rendered by get()
        return self.get(request, *args, **kwargs)
