import os

from django import template
from vacancy.models import ApplicationTask

register = template.Library()

@register.filter()
def filename(value):
    return os.path.basename(value.ApplicationTask.sourcefile.name)