from django.conf.urls import patterns, include, url

from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
# from django.conf import settings
from . import models
from . import views

urlpatterns = patterns('django.views.generic.date_based',

        url(r'^(?P<slug>.*?)/$',views.VacancyDetailView.as_view(), name='vacancy_detail'),

        url(r'^$',views.vacancy_list, name='vacancy_list'),

)
