from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
import functools
from modeltranslation.translator import translator, TranslationOptions
from ckeditor.fields import RichTextField
from core.models import User
import datetime
import os.path

# Create your models here.


class Vacancy(models.Model):
    name = models.CharField(_('Vacancy Name'),max_length=255)
    slug = models.CharField(_('Slug'),max_length=255)
    location =models.CharField(_('Location'),max_length=255)
    salary_description = models.CharField(_('Salary Description'),max_length=255)
    education = models.CharField(_('Education'),max_length=255)
    experience = models.CharField(_('Experience'),max_length=255)
    job_description = models.TextField(_('Job Description'),max_length=255)

    def __unicode__(self):
        return self.name

class ApplicationStatus(models.Model):
    name = models.CharField(_('Name'),max_length=255)
    code = models.CharField(_('Code'),max_length=40)
    def __unicode__(self):
        return self.name

class VacancyApplication(models.Model):
    vacancy = models.ForeignKey(Vacancy)
    userid = models.ForeignKey(User)
    cv = models.FileField(_('CV'),upload_to = 'vacancy/cv/')
    message = models.CharField(_('Message'),max_length=255)

    senddate = models.DateTimeField(_('Send Date'),auto_now_add=True)
    status = models.ForeignKey(ApplicationStatus)

    def __unicode__(self):
        return self.vacancy

class ApplicationTask(models.Model):
    applicationid = models.ForeignKey(VacancyApplication, related_name='tasks')
    name = models.CharField(_('Name'),max_length=255)
    sourcefile = models.FileField(_("SourceFile"),upload_to = 'vacancy/sourcfile/', blank=True)
    deadline = models.DateTimeField(_('Deadline'))
    resultdate = models.DateTimeField(_('ResultDate'))
    resultfile = models.FileField(_('ResultFile'),upload_to = 'vacancy/resultfile/', blank=True)

    def __unicode__(self):
        return self.name
    # def sourcefilename(self):
    #     return os.path.basename(self.sourcefile.name)
    # def resultfilename(self):
    #     return os.path.basename(self.resultfile.name)

class ApplicationInterview(models.Model):
    applicationid = models.ForeignKey(VacancyApplication)
    date = models.DateTimeField(_('Date'))
    connection = models.CharField(_('Connection'),max_length=255)
    comment = models.CharField(_('Comment'),max_length=255)

    # def __unicode__(self):
    #     return self.applicationid

class JobApplicationMessage(models.Model):
    userid=models.ForeignKey(User, blank=True, null=True)
    applicationid = models.ForeignKey(VacancyApplication)
    senddate =models.DateTimeField(_('Send Date'),auto_now_add=True)
    message = models.TextField(_('Message'))

    class Meta:
        verbose_name = _('Application Message')
        verbose_name_plural = _('Application Messages')
        ordering = ('-senddate', )

    def __unicode__(self):
        return self.message




