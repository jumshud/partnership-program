
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from django.contrib import admin
from models import Vacancy,ApplicationTask,ApplicationInterview,JobApplicationMessage
from forms import VacancyForm,ApplicationTaskForm,ApplicationInterviewForm,JobApplicationMessageForm

# Register your models here.

class VacancyAdmin(admin.ModelAdmin):
    form = VacancyForm
    # class Media:
        # js = ('ckeditor/ckeditor.js',)

#     """
#         Admin for services
#     """
#     # date_hierarchy = 'pub_date'
    list_display = ('name', 'location','salary_description')
#     #list_editable = ('title', 'is_published')
#     # list_filter = ('is_published', )
#     # search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('name',)}
#     # form = LibraryForm

class VacancyApplicationTaskAdmin(admin.ModelAdmin):
    form = ApplicationTaskForm

class ApplicationInterviewAdmin(admin.ModelAdmin):
    form = ApplicationInterviewForm

class JobApplicationMessageAdmin(admin.ModelAdmin):
    form = JobApplicationMessageForm

admin.site.register(Vacancy,VacancyAdmin)
admin.site.register(JobApplicationMessage,JobApplicationMessageAdmin)
admin.site.register(ApplicationTask,VacancyApplicationTaskAdmin)
admin.site.register(ApplicationInterview,ApplicationInterviewAdmin)
