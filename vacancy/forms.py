from __future__ import absolute_import
from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
# from cms.plugins.text.settings import USE_TINYMCE
from .models import Vacancy,VacancyApplication,ApplicationTask,ApplicationInterview,JobApplicationMessage
from ckeditor.widgets import CKEditorWidget
from django.forms import TextInput, Textarea

# from pages.translation import PageTranslationOptions
import datetime
#
class VacancyForm(forms.ModelForm):

    job_description = forms.CharField(widget=CKEditorWidget(attrs={'cols': 100, 'rows': 25}))
    job_description_en = forms.CharField(widget=CKEditorWidget(attrs={'cols': 100, 'rows': 25}))
    job_description_ru = forms.CharField(widget=CKEditorWidget(attrs={'cols': 100, 'rows': 25}))
    job_description_tr = forms.CharField(widget=CKEditorWidget(attrs={'cols': 100, 'rows': 25}))

    class Meta:
        model = Vacancy
#
class VacancyApplicationForm(forms.ModelForm):
    # status = forms.ForeignKey(required=False)

    class Meta:
        model = VacancyApplication
        fields =['vacancy','userid','cv','message','status']

class ApplicationTaskForm(forms.ModelForm):
    class Meta:
        model = ApplicationTask
        fields = ['resultfile']


class ApplicationInterviewForm(forms.ModelForm):
    class Meta:
        model = ApplicationInterview

class JobApplicationMessageForm(forms.ModelForm):

    class Meta:
        model = JobApplicationMessage
        fields = ['message']
        widgets = {
          'message': forms.Textarea(attrs={'style':'resize:none;height:80px;min-height:80px;'}),
        }
