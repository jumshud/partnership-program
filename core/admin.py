from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from core.models import Country, PartnerCategory, Partner, User, AccessLevel,\
                    PartnershipProgramMaterial, PartnershipRequestStatus, PartnershipRequest


from core.forms import CustomUserChangeForm, CustomUserCreationForm

class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference the removed 'username' field
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name','last_name', 'country')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
    list_display = ('email','partner', 'first_name','last_name', 'is_staff')
    search_fields = ('email', 'first_name','last_name',)
    ordering = ('email',)

admin.site.register(User, CustomUserAdmin)

class CountryAdmin(admin.ModelAdmin):
    list_display = ('name','code')
    search_fields = ('name', 'code')
admin.site.register(Country, CountryAdmin)


class PartnerCategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)
admin.site.register(PartnerCategory, PartnerCategoryAdmin)

class PartnerAdmin(admin.ModelAdmin):
    list_display = ('partner_email', 'country', 'city', 'company', 'address', 'phone', 'skype', 'website', 'zipcode', 'fax')
    search_fields = ('category', 'company', 'country')
    filter_horizontal = ('category',)
admin.site.register(Partner, PartnerAdmin)

