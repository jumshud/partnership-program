# Http
from django.http import HttpResponse, Http404,HttpResponseRedirect
#json
from django.core.serializers.json import DjangoJSONEncoder
import json
#mail
from django.core.mail import send_mail
#settings
from universus_partnership.settings import EMAIL_HOST_USER, PORTAL_URL
# models
from core.models import Partner, PartnerCategory, PartnershipProgram, User, UserProfile,PartnerPartnerCategory,UserPasswordRecovery
#form
from core.forms import RegisterForm, LoginForm, UpdatePartnerForm, UpdateUserForm, UserRegisterForm, PasswordChangeForm
# Template
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
#python
import datetime, random, sha
# Authentication
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout


# home page
def index(request):
    part_program = PartnershipProgram.objects.order_by("-update_date")[:4]

    return render_to_response('base.html',locals(), context_instance=RequestContext(request))


def partner_register(request):
    form = RegisterForm()
    if request.method == 'POST':
        form = RegisterForm(request.POST, request.FILES)
        user = User()
        # when form is valid register user
        if form.is_valid():
            partner = form.save()

            #save user
            user.email = form.cleaned_data["user_email"]
            user.last_name = form.cleaned_data["last_name"]
            user.first_name = form.cleaned_data["first_name"]
            user.position = form.cleaned_data["user_position"]
            user.partner = partner
            user.country = partner.country
            user.city = partner.city
            user.type = 'P'
            user.manager=True
            # set password for user and set deactive user
            user.set_password(form.cleaned_data["password1"])
            user.is_active = False
            user.save()
            salt = sha.new(str(random.random())).hexdigest()[:5]
            activation_key = sha.new(salt+user.first_name).hexdigest()
            key_expires = datetime.datetime.today() + datetime.timedelta(2)

            # Create and save their profile
            new_profile = UserProfile(user=user,
                                      activation_key=activation_key,
                                      key_expires=key_expires)
            new_profile.save()
            confirm_url = redirect('core:user_confirm', new_profile.activation_key)['Location']
            # Send an email with the confirmation link
            email_subject = 'Your new partnership account confirmation'
            email_body = "\tHello, %s, and thanks for signing up for an account!\n\n \
            To activate your account, click this link within 48  hours:\n\n %s%s" % (
                                                                                    user.first_name,
                                                                                    PORTAL_URL,
                                                                                    confirm_url[1:]) #todo confirm url
            send_mail(email_subject,
                      email_body,
                      EMAIL_HOST_USER,
                      [user.email])
            form = RegisterForm()
            return render_to_response('partner/register.html',
                                      {'message_sent': True},
                                      context_instance=RequestContext(request))

    return render_to_response('partner/register.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )


#Confirm user registration with email
def user_confirm(request, activation_key):
    user_profile = get_object_or_404(UserProfile,
                                     activation_key=activation_key)
    key_expires = None
    if user_profile.user.is_active:
        return render_to_response('partner/confirm.html', {'has_account': True}, context_instance=RequestContext(request))

    key_expires = user_profile.key_expires.replace(tzinfo=None)
    if key_expires < datetime.datetime.today():
        return render_to_response('partner/confirm.html', {'expired': True})
    user_account = user_profile.user

    user_account.is_active = True
    user_account.save()
    return render_to_response('partner/confirm.html', {'success': True,'user':user_account}, context_instance=RequestContext(request))

#profile view for login user
@login_required
def profile_info(request):
    user = request.user
    partner = user.partner
    return render_to_response('partner/profile_info.html',
                                  {'user':user, 'partner':partner},
                                  context_instance=RequestContext(request)
                              )


def user_login(request):
    req_url=str(request.GET.get('next',''))
    # sticks in a POST or renders empty form
    form = LoginForm(request.POST or None)
    # when form is valid login user
    if form.is_valid():
        next = request.POST.get('next',False)
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        user = authenticate(username=username, password=password)
        login(request, user)
        # Redirect to a success page.
        if next:
            return redirect(next)
        return redirect('core:index')

    return render_to_response('partner/login.html',
                                  {'form': form, 'req_url':req_url},
                                  context_instance=RequestContext(request)
                              )

#update profile information
@login_required
def profile_update(request):
    user = request.user
    partner = user.partner

    userForm =None
    partnerForm =None

    if request.method == 'POST':
        partnerForm = UpdatePartnerForm(request.POST,request.FILES, instance=partner)
        userForm = UpdateUserForm(request.POST, instance=user)
        if userForm.is_valid() and not partnerForm.is_valid() and not request.user.manager:
            update_user = userForm.save()
            update_user.phone = userForm.cleaned_data['user_phone']
            update_user.save()
            return redirect('core:profile_info')
        elif partnerForm.is_valid() and userForm.is_valid():
            update_partner = partnerForm.save()
            update_user = userForm.save()
            update_user.phone = userForm.cleaned_data['user_phone']
            update_user.country = update_partner.country
            update_user.city = update_partner.city
            update_user.save()
            return redirect('core:profile_info')

    else:
        partnerForm = UpdatePartnerForm(instance=partner)
        userForm = UpdateUserForm(instance=user, initial={'user_phone':user.phone})



    return render_to_response('partner/profile_update.html',
                                 {'partnerForm': partnerForm, 'userForm':userForm},
                                 context_instance=RequestContext(request)
                              )

# logout current user
@login_required
def user_logout(request):
    logout(request)
    return redirect('core:index')

#change user password
@login_required
def userPasswordChange(request):
    user = request.user
    # sticks in a POST or renders empty form
    #passing user and data varriable PasswordChangeForm
    form = PasswordChangeForm(user = request.user, data = request.POST or None)
    # when form is valid change password
    if form.is_valid():
        user.set_password(form.cleaned_data["password1"])
        user.save()
        return redirect('core:profile_info')

    return render_to_response('partner/change_password.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )
# Userrrrr
from vacancy.models import VacancyApplication,Vacancy,ApplicationTask,ApplicationInterview,JobApplicationMessage

from django.views import generic as generic_views
from django.utils import translation
from django.db.models import Q
from django.views.generic.edit import FormMixin
from django.views.generic.list import ListView
from vacancy.forms import ApplicationTaskForm,JobApplicationMessageForm
from django.db.models import Count

#user register view
def user_register(request):
    if request.method == 'POST':
        user_form =UserRegisterForm(request.POST)
        user = User()
        # when form is valid register user
        if user_form.is_valid():
            userform = user_form.save()
            userform.set_password(user_form.cleaned_data["password1"])
            userform.type = 'U'
            userform.save()
            username = user_form.cleaned_data["email"]
            password = user_form.cleaned_data["password1"]
            user = authenticate(username=username, password=password)
            login(request, user)

            return HttpResponseRedirect('/')
    else:
        user_form = UserRegisterForm()

    return render_to_response('partner/user_register.html',
                                  {'form': user_form},
                                  context_instance=RequestContext(request)
                              )

# User Profile View
@login_required
def user_profile(request):
    applications = VacancyApplication.objects.filter(userid_id=request.user.id)
    task = ApplicationTask.objects.all()

    tasks =[]
    for id in applications:
        tasks = id

    taskscount =ApplicationTask.objects.filter(applicationid = tasks).annotate(count=Count('applicationid'))

    # vacancy = Vacancy.objects.filter(id=applications.vacancy_id)
    value =5
    return render_to_response('vacancy/user_profile.html',{'value':value,'applications':applications,'task':task,
                                                           'taskscount':taskscount,'tasks':tasks}
                              ,context_instance=RequestContext(request))

class ApplicationPageView(FormMixin, ListView):
    model = Vacancy
    template_name = 'vacancy/application_page.html'
    form_class = ApplicationTaskForm
    form_class2 = JobApplicationMessageForm
    # success_url = '/thanks/'

    def get(self, request, id,*args, **kwargs):
     # From ProcessFormMixin
        form = self.form_class()
        form2 = self.form_class2()

        self.object_list = self.get_queryset()
        context = self.get_context_data(object_list=self.object_list, form=form,form2=form2)
        # context = super(ApplicationPageView,self).get_context_data(**kwargs)
        context['id'] = VacancyApplication.objects.all()
        vacancy_obj = VacancyApplication.objects.get(id=int(self.kwargs['id']))
        context['vacancy'] = vacancy_obj

        application_task = ApplicationTask.objects.filter(applicationid = self.kwargs['id'])
        context['tasks'] = application_task

        interviews = ApplicationInterview.objects.filter(applicationid = self.kwargs['id'])
        context['interviews'] = interviews

        msg = JobApplicationMessage.objects.filter(applicationid = self.kwargs['id'])
        context['msg'] = msg

        # context['vacancy_list']=Vacancy.objects.filter(~Q(slug=self.kwargs['slug']))
        cur_language = translation.get_language()
        context['cur_language'] = cur_language
        context['default_url'] = 1
        # context['x'] = self.request.user.id
        context['y'] = vacancy_obj.id


        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        instance = ApplicationTask.objects.get(id=int(request.POST.get("id","")))
        form = self.form_class(request.POST, request.FILES, instance=instance)
        if form.is_valid():
            form.save()
            # self.form_class(request.POST, request.FILES,instance=instance)
            return HttpResponseRedirect('.')

        else:
            return HttpResponseRedirect('.')


def message_form(request):
     if request.method == 'POST':
        form =JobApplicationMessageForm(request.POST)

        # user = User()
        # when form is valid register user
        if form.is_valid():
            form = form.save(commit=False)
            # message = form.save()
            form.userid = request.user
            form.applicationid_id = request.POST.get('applicationid','')
            form.save()
            return redirect('core:application_page', request.POST.get('applicationid',''))
     else:
        form = JobApplicationMessageForm()

     return render_to_response('vacancy/application_page.html',
                                  {'form2': form},
                                  context_instance=RequestContext(request)
                              )


    # def post2(self, request, *args, **kwargs):
    #     # instance = ApplicationTask.objects.get(id=request.POST.get("id",""))
    #     form2 = self.form_class2(request.POST)
    #     if form2.is_valid():
    #         form2.save()
    #         # self.form_class(request.POST, request.FILES,instance=instance)
    #         return HttpResponseRedirect('.')
    #     else:
    #         return HttpResponseRedirect('.')
