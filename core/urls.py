from django.conf.urls import patterns, url
from core import views
from workspace import views as wp_views


urlpatterns = patterns('',
    url(r'^$', views.index,  name='index'),
    #user actions
    url(r'^user-registration/$', views.user_register,  name='user_registration'),
    url(r'^partner/registration/$', views.partner_register,  name='registration'),
    url(r'^account/login/$',   views.user_login,  name='user_login'),
    url(r'^partner/profile/update/$',   views.profile_update,  name='profile_update'),
    url(r'^partner/profile/$',   views.profile_info,  name='profile_info'),
    url(r'^account/logout/$',   views.user_logout,  name='user_logout'),
    url(r'^account/password/change/$',   views.userPasswordChange,  name='password_change'),

    url(r'^user/application-page/message/$',views.message_form,name='message_form'),

    url(r'^user/application-page/(?P<id>.*?)/$',views.ApplicationPageView.as_view(),name='application_page'),

    url(r'^user/$',views.user_profile,name='user_profile'),


    url(r'^confirm/(?P<activation_key>[-\w]+)/$',   views.user_confirm,  name='user_confirm'),

    #partnership panel
    url(r'^ppanel/$',  wp_views.partnership_panel,  name='partnership_panel'),
    url(r'^ppanel/employee/(?P<id>[-\w]+)/$',  wp_views.employee_info,  name='employee_info'),
    url(r'^ppanel/workspace/view/(?P<id>.*?)/$',  wp_views.workspace_detail,  name='workspace_detail'),
)
