from django.core.paginator import Paginator, InvalidPage, EmptyPage

def get_pages(request,items, page_size):
    paginator = Paginator(items, page_size)
    # Make sure page request is an int. If not, deliver first page.
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        pages = paginator.page(page)
    except (EmptyPage, InvalidPage):
        pages = paginator.page(paginator.num_pages)

    return pages


def paginate(DISPLAY_PAGES_COUNT, page_count, current_page):
    page_list = []

    MIN_COUNT = 5
    if page_count <= DISPLAY_PAGES_COUNT:
        page_list = range(1, page_count + 1)
    elif page_count == 10:
        if current_page < MIN_COUNT:
            page_list= range(1, MIN_COUNT + 1)
            page_list.append(-1)
            page_list.append(page_count-1)
            page_list.append(page_count)
        elif current_page >MIN_COUNT:
            page_list = [1,2]
            page_list.append(-1)
            if current_page == (MIN_COUNT + 1):
                page_list += range(MIN_COUNT, page_count+1)
            else:
                page_list += range(MIN_COUNT+1, page_count+1)
        else:
            page_list = [1,2,-1]
            page_list += range(current_page-1, current_page+3)
            page_list.append(-1)
            page_list.append(page_count)
    else:
        if current_page < MIN_COUNT-1:
            page_list= range(1, MIN_COUNT + 1)
            if page_count > MIN_COUNT+2:
                page_list.append(-1)
            page_list += range(page_count-1, page_count + 1)
        elif current_page == MIN_COUNT-1:
            page_list= range(1, MIN_COUNT + 2)
            if page_count > MIN_COUNT + 3:
                page_list.append(-1)
                page_list +=range(page_count-1, page_count + 1)
            else:
                page_list +=range(MIN_COUNT + 2, page_count + 1)
        elif current_page == MIN_COUNT:
            page_list = [1,2,-1]
            page_list += range(current_page-1, current_page + 3)
            if page_count > current_page+2:
                page_list.append(-1)
                page_list += range(page_count, page_count + 1)
        elif current_page > MIN_COUNT and current_page <= (page_count - MIN_COUNT):
            page_list = [1,2,-1]
            page_list += range(current_page-2, current_page + 3)
            page_list.append(-1)
            page_list += range(page_count-1, page_count + 1)
        else:
            page_list = [1,2]
            if page_count > MIN_COUNT+2:
                page_list.append(-1)
            if current_page == (page_count - MIN_COUNT + 1):
                page_list += range(page_count - MIN_COUNT, page_count+1)
            else:
                page_list += range(page_count - MIN_COUNT + 1, page_count+1)
    return page_list