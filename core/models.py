from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.contrib.auth.hashers import (
    check_password, make_password, is_password_usable)

from django.dispatch import receiver


class CustomUserManager(BaseUserManager):
    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError(_('The given email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class Country(models.Model):
    name = models.CharField(_('Name'), max_length=100, blank=False)
    code = models.CharField(_('Code'),max_length=10, blank=False)

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')
        ordering = ('name', )

    def __unicode__(self):
        return self.name


class PartnerCategory(models.Model):
    name = models.CharField(_('Category Name'),max_length=80, blank=False)

    class Meta:
        verbose_name = _('Partner Category')
        verbose_name_plural = _('Partner Categories')
    def __unicode__(self):
        return self.name

class Partner(models.Model):
    partner_email = models.EmailField(_('Company Email'), unique=True, db_index=True, blank=True, null=True)
    country = models.ForeignKey(Country, related_name='partners', null=True, blank=True)
    city = models.CharField(_('City'), max_length=100, null=True, blank=True)
    company = models.CharField(_('Company'), max_length=100, null=True, blank=True)
    address = models.CharField(_('Address'), max_length=150, null=True, blank=True)
    phone = models.CharField(_('Phone'), max_length=20, null=True, blank=True)
    skype = models.CharField(_('Skype'), max_length=50, null=True, blank=True)
    website = models.CharField(_('Web site'), max_length=60, null=True, blank=True)
    zipcode = models.CharField(_('ZIP code'), max_length=40, blank=True, null=True)
    fax = models.CharField(_('Fax'), max_length=40, blank=True, null=True)
    logo = models.ImageField(_('Image'), upload_to='partners/', blank=True)
    # partner category
    category = models.ManyToManyField(PartnerCategory, _('Partner Category'), through='PartnerPartnerCategory', related_name='partners', null=True, blank=True)

    class Meta:
        verbose_name = _('Partner')
        verbose_name_plural = _('Partners')

    def __unicode__(self):
        return self.company

class PartnerPartnerCategory(models.Model):
    access_level = models.CharField(_('Access Level'),max_length=10, blank=True)
    partner = models.ForeignKey(Partner, related_name='partners', null=True, blank=True)
    category = models.ForeignKey(PartnerCategory, related_name='partner_category', null=True, blank=True)



class User(AbstractBaseUser, PermissionsMixin):
    partner = models.ForeignKey(Partner, related_name='users', blank=True, null=True)
    #User personal info
    manager = models.BooleanField(_('Manager'), default=False)
    email = models.EmailField(_('Email'), unique=True, db_index=True)
    first_name = models.CharField(_('First Name'), max_length=120, blank=False)
    last_name = models.CharField(_('Last Name'), max_length=120, blank=False)
    position = models.CharField(_('Position'), max_length=100, blank=False)
    country = models.ForeignKey(Country, related_name='users', null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    company = models.CharField(max_length=100, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    skype = models.CharField(max_length=50, null=True, blank=True)
    date_joined = models.DateTimeField(_('Date joined'), default=timezone.now)
    type = models.CharField(_('Type'),max_length=10, blank=True, null=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = []

    def __unicode__(self):
        return self.email

    # def get_absolute_url(self):
    #     return reverse('register:user_profile', kwargs={
    #                                 'id': self.id})

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

#This class has used to confirm registration
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    activation_key = models.CharField(max_length=50)
    key_expires = models.DateTimeField()

#This class has used to set new password when user forget password
class UserPasswordRecovery(models.Model):
    user = models.OneToOneField(User)
    activation_key = models.CharField(max_length=50)
    key_expires = models.DateTimeField()


class PartnershipProgram(models.Model):
    name = models.CharField(_('Name'), max_length=120, blank=False)
    slug = models.SlugField(_('Slug'), unique=True)
    desc = models.TextField(_('Description'), blank=True, null=True)
    image = models.ImageField(_('Image'), upload_to='partnership program/')
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now_add=True)
    category = models.ManyToManyField(PartnerCategory, _('Partner Category'), related_name='partnershipPrograms', null=True, blank=True)

    class Meta:
        verbose_name = _('Partnership Program')
        verbose_name_plural = _('Partnership Programs')
        ordering = ('-update_date',)

    def __unicode__(self):
        return self.name


class AccessLevel(models.Model):
    name = models.CharField(_('Name'),max_length=80, blank=False)

    class Meta:
        verbose_name = _('Access Level')
        verbose_name_plural = _('Access Levels')

    def __unicode__(self):
        return self.name

class PartnershipProgramMaterial(models.Model):
    name = models.CharField(_('Name'), max_length=55, blank=False)
    desc = models.TextField(_('Description'), blank=True, null=True)
    text = models.TextField(_('Text'), blank=True, null=True)
    file = models.FileField(upload_to='program_material', blank=True, null=True)
    access_level = models.ForeignKey(AccessLevel, related_name='programmaterials', blank=False, null=True)

    class Meta:
        verbose_name = _('Partnership Program Material')
        verbose_name_plural = _('Partnership Program Materials')

    def __unicode__(self):
        return self.name


class PartnershipRequestStatus(models.Model):
    name = models.CharField(_('Name'), max_length=55, blank=False)
    def __unicode__(self):
        return self.name

class PartnershipRequest(models.Model):
    partner = models.ForeignKey(Partner, related_name='requests', blank=True, null=True)
    partnership_program = models.ForeignKey(PartnershipProgram, related_name='requests', blank=True, null=True)
    send_date = models.DateTimeField(auto_now_add=True)
    status = models.ForeignKey(PartnershipRequestStatus, related_name='requests', blank=False)

    class Meta:
        verbose_name = _('Partnership Request')
        verbose_name_plural = _('Partnership Requests')

    def __unicode__(self):
        return self.name

