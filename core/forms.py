from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from core.models import User, Partner, PartnerCategory
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth import authenticate, login, logout

from django import forms
from django.forms import ModelForm

class CustomUserCreationForm(UserCreationForm):
    """
    A form that creates a user, with no privileges, from the given email and
    password.
    """

    def __init__(self, *args, **kargs):
        super(CustomUserCreationForm, self).__init__(*args, **kargs)
        del self.fields['username']

    class Meta:
        model = User
        fields = ("email",)

class CustomUserChangeForm(UserChangeForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    def __init__(self, *args, **kargs):
        super(CustomUserChangeForm, self).__init__(*args, **kargs)
        del self.fields['username']

    class Meta:
        model = User


#user register. added by mubariz
class UserRegisterForm(ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput, label=_('Password'), max_length=50)
    password2 = forms.CharField(widget=forms.PasswordInput, label=_('Confirm Password'), max_length=50)
    class Meta:
        model = User
        fields = ['email','password1', 'password2', 'first_name', 'last_name', 'phone',
                  'country','city','skype',]

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data.get("password2", "")
        if password1 and password2:  # If both passwords has value
            if password1 != password2:
                raise forms.ValidationError(_(u"Passwords didn't match."))
        else:
            raise forms.ValidationError(_(u"Passwords can't be blank."))
        return password2


#partner register
class RegisterForm(ModelForm):
    first_name = forms.CharField(label=_('First Name'), max_length=50)
    last_name = forms.CharField(label=_('Last Name'), max_length=50)
    user_email = forms.EmailField(label=_('Your Email'))
    user_phone = forms.CharField(label=_('Your Phone'), max_length=50, required=False)
    user_position = forms.CharField(label=_('Your Position'), max_length=50, required=False)
    password1 = forms.CharField(widget=forms.PasswordInput, label=_('Password'), max_length=50)
    password2 = forms.CharField(widget=forms.PasswordInput, label=_('Confirm Password'), max_length=50)


    class Meta:
        model = Partner
        fields = ['user_email','password1', 'password2', 'first_name', 'last_name', 'user_phone',
                  'country', 'city', 'company', 'address', 'zipcode', 'partner_email', 'phone', 'skype', 'website', 'fax', 'logo']

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data.get("password2", "")
        if password1 and password2:  # If both passwords has value
            if password1 != password2:
                raise forms.ValidationError(_(u"Passwords didn't match."))
        else:
            raise forms.ValidationError(_(u"Passwords can't be blank."))
        return password2

    def clean_user_email(self):
        user_email = self.cleaned_data.get("user_email", "")
        user = User.objects.filter(email=user_email)
        if user:
            raise forms.ValidationError(_(u"User with this email already exist."))
        return user_email


class UpdatePartnerForm(ModelForm):
    class Meta:
        model = Partner
        fields = ['country', 'city', 'company', 'address', 'zipcode', 'partner_email', 'phone','skype','website','fax','logo']


class UpdateUserForm(ModelForm):
    user_phone = forms.CharField(label=_('Your Phone'), required=False, max_length=50)
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'user_phone', 'position']

class LoginForm(forms.Form):
    username = forms.EmailField(label=_('Username'))
    password = forms.CharField(widget=forms.PasswordInput,
                                label=_('Password'),
                                max_length=50)


    class Meta:
        fields = ['username','password']

    def clean_password(self):
        username = self.cleaned_data.get("username", "")
        password = self.cleaned_data.get("password", "")
        user = authenticate(username=username, password=password)
        if user is None:
            raise forms.ValidationError(_(u"Username or Passwords is not valid."))
        elif not user.is_active:
            raise forms.ValidationError(_(u"This user is not active"))
        return password


#This form is used for change password current user.
class PasswordChangeForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput,
                                label=_('Current Password'),
                                max_length=50, required=True)
    password1 = forms.CharField(widget=forms.PasswordInput,
                                label=_('New Password'),
                                max_length=50, required=True)
    password2 = forms.CharField(widget=forms.PasswordInput,
                                label=_('Confirm Password'),
                                max_length=50, required=True)


    class Meta:
        fields = ['password','password1', 'password2']

    def __init__(self, user, data=None, *args, **kwargs):
        self.user = user
        self.data = data
        super(PasswordChangeForm, self).__init__(data=data, *args, **kwargs)

    #validating old password.
    def clean_password(self):
        password = self.cleaned_data.get('password', None)
        if not self.user.check_password(password):
            raise forms.ValidationError(_('Current password is wrong.'))
        return password

    #checking both password exist and same.
    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data.get("password2", "")
        if password1 and password2:  # If both passwords has value
            if password1 != password2:
                raise forms.ValidationError(_(u"Passwords didn't match."))
        else:
            raise forms.ValidationError(_(u"Passwords can't be blank."))
        return password2
