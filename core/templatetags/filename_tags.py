import os

from django import template
from vacancy.models import ApplicationTask

register = template.Library()

@register.filter()
def sourcefilename(sourcefile):
    return os.path.basename(sourcefile.name)

@register.filter()
def resultfilename(resultfile):
    return os.path.basename(resultfile.name)

