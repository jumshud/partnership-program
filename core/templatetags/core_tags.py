from django import template
from django.db.models import get_model
from news.models import News
from core.models import PartnershipRequest

register = template.Library()

@register.filter()
def get_latest_news(check):
    return News.published.all()[:6]


@register.filter()
def check_request_path(request_path):
    req_array = request_path.split('/')
    if len(req_array) == 3:
        return True
    return False

@register.filter()
def get_program_status(program,partner):
    partnership_req = PartnershipRequest.objects.filter(partner=partner, partnership_program=program)
    if partnership_req:
        return partnership_req[0].status.name
    return 'None'

