from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from workspace.models import WorkspaceMessage, Workspace, WorkspaceFile
from core.models import User
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.forms import ModelForm


class WorkspaceForm(ModelForm):
    class Meta:
        model = Workspace
        fields = ['name', 'description']
        widgets = {
          'description': forms.Textarea(attrs={'style':'resize:none;height:60px;min-height:60px;'}),
        }

class WorkspaceFileForm(ModelForm):
    class Meta:
        model = WorkspaceFile
        fields = ['name', 'comment', 'file']
        widgets = {
          'comment': forms.Textarea(attrs={'style':'resize:none;height:60px;min-height:60px;'}),
        }

class WorkspaceMessageForm(ModelForm):
    class Meta:
        model = WorkspaceMessage
        fields = ['message']
        widgets = {
          'message': forms.Textarea(attrs={'style':'resize:none;height:80px;min-height:80px;'}),
        }


#partner register
class PanelUserRegisterForm(ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput, label=_('Password'), max_length=50)
    password2 = forms.CharField(widget=forms.PasswordInput, label=_('Confirm Password'), max_length=50)

    class Meta:
        model = User
        fields = ['email','password1', 'password2', 'first_name', 'last_name', 'phone', 'position',
                  'country', 'city', 'company', 'skype']

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data.get("password2", "")
        if password1 and password2:  # If both passwords has value
            if password1 != password2:
                raise forms.ValidationError(_(u"Passwords didn't match."))
        else:
            raise forms.ValidationError(_(u"Passwords can't be blank."))
        return password2
