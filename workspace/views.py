# Http
from django.http import HttpResponse, Http404
# models
from core.models import User, PartnershipProgram, UserProfile
from workspace.models import Workspace, WorkspaceFile, WorkspaceMessage
# Template
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
# Authentication
from django.contrib.auth.decorators import login_required
#forms
from workspace.forms import WorkspaceForm, WorkspaceMessageForm, WorkspaceFileForm, PanelUserRegisterForm
#settings
from universus_partnership.settings import PORTAL_URL, EMAIL_HOST_USER, BASE_DIR
#mail
from django.core.mail import send_mail
#python
import datetime, random, sha
import os


@login_required
def partnership_panel(request):
    is_partner = False
    if request.user.partner:
        is_partner = True
    workspace_form = None
    user_form = None
    workspaces = Workspace.objects.filter(partner=request.user.partner)
    programs = PartnershipProgram.objects.all()
    partner = User.objects.get(manager=True, partner=request.user.partner)
    users = User.objects.filter(partner=request.user.partner, is_active=True).exclude(pk=partner.id)
    if request.method == 'POST':
        if 'description' in request.POST:
            workspace_form = WorkspaceForm(request.POST)
            if workspace_form.is_valid():
                ws_obj = workspace_form.save(commit=False)
                ws_obj.user=request.user
                ws_obj.partner=request.user.partner
                ws_obj.save()
                return redirect('core:partnership_panel')
        if 'email' in request.POST:
            user_form = PanelUserRegisterForm(request.POST)
            if user_form.is_valid():
                user = user_form.save(commit=False)
                user.partner=request.user.partner

                user.type = 'P'
                # set password for user and set deactive user
                user.set_password(user_form.cleaned_data["password1"])
                user.is_active = False
                user.save()
                salt = sha.new(str(random.random())).hexdigest()[:5]
                activation_key = sha.new(salt+user.first_name).hexdigest()
                key_expires = datetime.datetime.today() + datetime.timedelta(2)

                # Create and save their profile
                new_profile = UserProfile(user=user,
                                          activation_key=activation_key,
                                          key_expires=key_expires)
                new_profile.save()
                confirm_url = redirect('core:user_confirm', new_profile.activation_key)['Location']
                # Send an email with the confirmation link
                email_subject = 'Your new partnership account confirmation'
                email_body = "\tHello, %s, and thanks for signing up for an account!\n\n \
                To activate your account, click this link within 48  hours:\n\n %s%s" % (
                                                                                        user.first_name,
                                                                                        PORTAL_URL,
                                                                                        confirm_url[1:])
                send_mail(email_subject,
                          email_body,
                          EMAIL_HOST_USER,
                          [user.email])

                return redirect('core:partnership_panel')
                return render_to_response('partner/employee_register.html',
                      {'message_sent': True},
                      context_instance=RequestContext(request))
    else:
        workspace_form = WorkspaceForm()
        user_form = PanelUserRegisterForm()

    return render_to_response('partner/partnership_panel.html',
                              {'is_partner': is_partner,'workspaces':workspaces, 'programs':programs, 'users':users,
                               'workspace_form':workspace_form, 'user_form':user_form},
                              context_instance=RequestContext(request))


@login_required
def workspace_detail(request, id):
    is_partner = True
    if not request.user.partner:
        return render_to_response('partner/workspace_detail.html',
                              {'is_partner':False},
                              context_instance=RequestContext(request))
    workspace = Workspace.objects.get(pk=id)
    message_form = None
    file_form = None
    if request.method == 'POST':
        if 'message' in request.POST:
            message_form = WorkspaceMessageForm(request.POST)
            if message_form.is_valid():
                if message_form.cleaned_data['message']:
                    ws_message = message_form.save(commit=False)
                    ws_message.user = request.user
                    ws_message.workspace = workspace
                    ws_message.save()
                    return redirect('core:workspace_detail',workspace.id)
        elif 'comment' in request.POST and request.FILES:
            file_form = WorkspaceFileForm(request.POST, request.FILES)
            if file_form.is_valid():
                # from sendfile import sendfile
                # filepath = 'D://ev_quslari.docx'
                # return sendfile(request,filepath)

                ws_file = file_form.save(commit=False)
                ws_file.workspace = workspace
                ws_file.user = request.user
                ws_file.partner = request.user.partner
                ws_file.save()

                return redirect('core:workspace_detail',workspace.id)

    else:
        message_form=WorkspaceMessageForm()
        file_form = WorkspaceFileForm()
    return render_to_response('partner/workspace_detail.html',
                              {'is_partner':is_partner, 'workspace':workspace, 'message_form':message_form, 'file_form': file_form},
                              context_instance=RequestContext(request))



def employee_info(request, id):
    employee = User.objects.get(id=id)
    if request.user.partner == employee.partner:
        return render_to_response('partner/employee_info.html', locals(),
                              context_instance=RequestContext(request))
    else:
        return render_to_response('partner/employee_info.html', {'wrong_user':True},
                              context_instance=RequestContext(request))