from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.models import User, Partner
#signal
from django.dispatch import receiver
#mail
from django.core.mail import send_mail
#settings
from universus_partnership.settings import EMAIL_HOST_USER, PORTAL_URL, MEDIA_URL

import random, string
from django.contrib.contenttypes.models import ContentType

class Workspace(models.Model):
    name = models.CharField(_('Name'), max_length=150, blank=False)
    description = models.TextField(blank=True, null=True)
    user = models.ForeignKey(User, related_name='workspaces', blank=False)
    partner = models.ForeignKey(Partner, related_name='workspaces', blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    class Meta:
        verbose_name = _('Workspace')
        verbose_name_plural = _('Workspaces')
        ordering = ('-created_date', )

    def __unicode__(self):
        return self.name


def get_path(instance, filename):
    path = 'workspace/%s/' % instance.workspace.name
    extension = filename.split('.')[-1]
    path = path.replace(" ", "_")
    return '%s%s' % (path, filename)

class WorkspaceFile(models.Model):
    file = models.FileField(upload_to=get_path, blank=False)
    name = models.CharField(_('Name'), max_length=150, blank=False)
    workspace = models.ForeignKey(Workspace, related_name='files', blank=True, null=True)
    user = models.ForeignKey(User, related_name='wsfiles', blank=False)
    partner = models.ForeignKey(Partner, related_name='wsfiles', blank=True, null=True)
    send_date = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(blank=True,  null=True)
    class Meta:
        verbose_name = _('Workspace File')
        verbose_name_plural = _('Workspace files')

    def __unicode__(self):
        return self.name

class WorkspaceMessage(models.Model):
    user = models.ForeignKey(User, related_name='wsmessages', blank=False)
    workspace = models.ForeignKey(Workspace, related_name='messages', blank=False)
    message = models.TextField()
    send_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Workspace Message')
        verbose_name_plural = _('Workspace Messages')
        ordering = ('-send_date', )

    def __unicode__(self):
        return self.message



# send message when create or update WorkspaceFile object
# @receiver(models.signals.post_save, sender=WorkspaceFile)
# def auto_send_mail_on_create(sender, instance, **kwargs):
#     if instance.user.partner:
#         email_list =[user.email for user in User.objects.filter(partner=instance.user.partner).exclude(pk=instance.user.id)]
#         panel_url = 'ppanel/'
#         # Send an email with the confirmation link
#         email_subject = 'New file from Partnership Portal'
#         email_body = "\t%s added new file into partnership panel. \
#         Click this link to show file:\n\n %s%s" % (instance.user.get_full_name,
#                                                      PORTAL_URL,
#                                                      panel_url
#                                                     )
#         send_mail(email_subject,
#                   email_body,
#                   EMAIL_HOST_USER,
#                   email_list)